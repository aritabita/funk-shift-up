﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    private int randomInt;

    public Animator charAnim;
    public AudioSource AudioS;
    public AudioClip[] footsteps;

    // Use this for initialization
    void Start()
    {
        this.gameObject.GetComponent<AudioSource>();
        charAnim = this.gameObject.GetComponent<Animator>();

        charAnim.SetBool("Walking", false);
        charAnim.SetBool("Running", false);
        charAnim.SetBool("Healing", false);
    }

    // Update is called once per frame
    void Update()
    {
        charactermover();
    }

    void charactermover()
    {
        if (Input.GetKey(KeyCode.A))
        {

            if (MusicSwitch.Song1Active)
            {
                charAnim.SetBool("Running", true);
                charAnim.SetBool("Walking", false);
                charAnim.SetBool("Healing", false);
            }
            else if (MusicSwitch.Song2Active)
            {
                charAnim.SetBool("Healing", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Walking", false);
            }
            else if (MusicSwitch.Song0Active)
            {
                charAnim.SetBool("Walking", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Healing", false);
            }

        }

        else if (Input.GetKey(KeyCode.D))
        {            

            if (MusicSwitch.Song1Active)
            {
                charAnim.SetBool("Running", true);
                charAnim.SetBool("Walking", false);
                charAnim.SetBool("Healing", false);
            }
            else if (MusicSwitch.Song2Active)
            {
                charAnim.SetBool("Healing", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Walking", false);
            }
            else if (MusicSwitch.Song0Active)
            {
                charAnim.SetBool("Walking", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Healing", false);
            }
        }

        else if (Input.GetKey(KeyCode.W))
        {            

            if (MusicSwitch.Song1Active)
            {
                charAnim.SetBool("Running", true);
                charAnim.SetBool("Walking", false);
                charAnim.SetBool("Healing", false);
            }
            else if (MusicSwitch.Song2Active)
            {
                charAnim.SetBool("Healing", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Walking", false);
            }
            else if (MusicSwitch.Song0Active)
            {
                charAnim.SetBool("Walking", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Healing", false);
            }
        }

        else if (Input.GetKey(KeyCode.S))
        {            

            if (MusicSwitch.Song1Active)
            {
                charAnim.SetBool("Running", true);
                charAnim.SetBool("Walking", false);
                charAnim.SetBool("Healing", false);
            }
            else if (MusicSwitch.Song2Active)
            {
                charAnim.SetBool("Healing", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Walking", false);
            }
            else if (MusicSwitch.Song0Active)
            {
                charAnim.SetBool("Walking", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Healing", false);
            }
        }

        else
        {
            charAnim.SetBool("Walking", false);
            charAnim.SetBool("Running", false);
            charAnim.SetBool("Healing", false);
        }



    }

    void PlayFootstep()
    {
        randomInt = Random.Range(0, 4);

        if (randomInt == 0)
        {
            AudioS.pitch = Random.Range(0.9f, 1.2f);
            AudioS.PlayOneShot(footsteps[randomInt]);
        }
        else if (randomInt == 1)
        {
            AudioS.pitch = Random.Range(0.9f, 1.2f);
            AudioS.PlayOneShot(footsteps[randomInt]);
        }
        else if (randomInt == 2)
        {
            AudioS.pitch = Random.Range(0.9f, 1.2f);
            AudioS.PlayOneShot(footsteps[randomInt]);
        }
        else
        {
            AudioS.pitch = Random.Range(0.9f, 1.2f);
            AudioS.PlayOneShot(footsteps[randomInt]);
        }
    }

    void BlankStep()
    {
        //AudioS.Stop();
    }
}
