﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{

    public CharacterScript charin;
    public GameObject player;
    public Rigidbody2D bullet;

    public Animator gunKick;
    // Use this for initialization
    void Start()
    {
        charin = player.GetComponent<CharacterScript>();
    }

    // Update is called once per frame
    void Update()
    {
        Fire();
    }

    void Fire()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (MusicSwitch.Song1Active)
            {
                if (charin.N)
                {
                    Rigidbody2D bulletInstance = Instantiate(bullet) as Rigidbody2D;
                    bulletInstance.transform.parent = gameObject.transform;
                    bulletInstance.transform.localPosition = Vector3.zero;
                    bulletInstance.transform.localRotation = Quaternion.identity;
                    Physics2D.IgnoreCollision(bulletInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                    bulletInstance.velocity = new Vector2(0, charin.bulletspeed);

                    charin.GunFire();
                }

                if (charin.NE)
                {
                    Rigidbody2D bulletInstance = Instantiate(bullet) as Rigidbody2D;
                    bulletInstance.transform.parent = gameObject.transform;
                    bulletInstance.transform.localPosition = Vector3.zero;
                    bulletInstance.transform.localRotation = Quaternion.identity;
                    Physics2D.IgnoreCollision(bulletInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                    bulletInstance.velocity = new Vector2(charin.bulletspeed, charin.bulletspeed);

                    charin.GunFire();
                }

                if (charin.E)
                {
                    Rigidbody2D bulletInstance = Instantiate(bullet) as Rigidbody2D;
                    bulletInstance.transform.parent = gameObject.transform;
                    bulletInstance.transform.localPosition = Vector3.zero;
                    bulletInstance.transform.localRotation = Quaternion.identity;
                    Physics2D.IgnoreCollision(bulletInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                    bulletInstance.velocity = new Vector2(charin.bulletspeed, 0);

                    charin.GunFire();
                }
                if (charin.SE)
                {
                    Rigidbody2D bulletInstance = Instantiate(bullet) as Rigidbody2D;
                    bulletInstance.transform.parent = gameObject.transform;
                    bulletInstance.transform.localPosition = Vector3.zero;
                    bulletInstance.transform.localRotation = Quaternion.identity;
                    Physics2D.IgnoreCollision(bulletInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                    bulletInstance.velocity = new Vector2(charin.bulletspeed, -charin.bulletspeed);

                    charin.GunFire();
                }
                if (charin.S)
                {
                    Rigidbody2D bulletInstance = Instantiate(bullet) as Rigidbody2D;
                    bulletInstance.transform.parent = gameObject.transform;
                    bulletInstance.transform.localPosition = Vector3.zero;
                    bulletInstance.transform.localRotation = Quaternion.identity;
                    Physics2D.IgnoreCollision(bulletInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                    bulletInstance.velocity = new Vector2(0, -charin.bulletspeed);

                    charin.GunFire();
                }
                if (charin.SW)
                {
                    Rigidbody2D bulletInstance = Instantiate(bullet) as Rigidbody2D;
                    bulletInstance.transform.parent = gameObject.transform;
                    bulletInstance.transform.localPosition = Vector3.zero;
                    bulletInstance.transform.localRotation = Quaternion.identity;
                    Physics2D.IgnoreCollision(bulletInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                    bulletInstance.velocity = new Vector2(-charin.bulletspeed, -charin.bulletspeed);

                    charin.GunFire();
                }
                if (charin.W)
                {
                    Rigidbody2D bulletInstance = Instantiate(bullet) as Rigidbody2D;
                    bulletInstance.transform.parent = gameObject.transform;
                    bulletInstance.transform.localPosition = Vector3.zero;
                    bulletInstance.transform.localRotation = Quaternion.identity;
                    Physics2D.IgnoreCollision(bulletInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                    bulletInstance.velocity = new Vector2(-charin.bulletspeed, 0);

                    charin.GunFire();
                }
                if (charin.NW)
                {
                    Rigidbody2D bulletInstance = Instantiate(bullet) as Rigidbody2D;
                    bulletInstance.transform.parent = gameObject.transform;
                    bulletInstance.transform.localPosition = Vector3.zero;
                    bulletInstance.transform.localRotation = Quaternion.identity;
                    Physics2D.IgnoreCollision(bulletInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                    bulletInstance.velocity = new Vector2(-charin.bulletspeed, charin.bulletspeed);

                    charin.GunFire();
                }
            }
        }
    }
}