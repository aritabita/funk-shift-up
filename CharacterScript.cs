﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterScript : MonoBehaviour {
    private int randomInt;

    public static float speed = 3.0f;
    public bool N = false;
    public bool NE = false;
    public bool E = false;
    public bool SE = false;
    public bool S = false;
    public bool SW = false;
    public bool W = false;
    public bool NW = false;
    public float angle;
    public GameObject Shield;
    public GameObject Sword;
    public GameObject Gun;
    public Collider2D SwordCol;
    public Rigidbody2D bullet;
    public static Rigidbody2D characterrigid;
    public Collider2D charactercol;
    public SpriteRenderer charactersprite;
    public float bulletspeed = 0.01f;
    public static float HP;
    public Animator charAnim;
    public AudioSource AudioS;
    public AudioClip[] footsteps;
    public AudioSource gunFire;
    public AudioClip[] shots;
    public static bool isHurt;
    public static bool isAttacking;
    public Animator swordAnim;

    // Use this for initialization
    void Start ()
    {
        HP = 20;
        this.gameObject.GetComponent<AudioSource>();
        charactercol = gameObject.GetComponent<Collider2D>();
        charactersprite = gameObject.GetComponent<SpriteRenderer>();
        charAnim = this.gameObject.GetComponent<Animator>();
        characterrigid = gameObject.GetComponent<Rigidbody2D>();
        Shield.SetActive(false);
        Sword.SetActive(false);
        Gun.SetActive(false);
        SwordCol.enabled = false;
        charAnim.SetBool("Walking", false);
        charAnim.SetBool("Running", false);
        charAnim.SetBool("Healing", false);
        charAnim.SetBool("facingN", false);
        charAnim.SetBool("facingE", false);
        charAnim.SetBool("facingS", false);
        charAnim.SetBool("facingW", false);
        charAnim.SetBool("movingN", false);
        charAnim.SetBool("movingE", false);
        charAnim.SetBool("movingS", false);
        charAnim.SetBool("movingW", false);

        swordAnim.SetBool("isSwinging", false);

    }
	
	// Update is called once per frame
	void Update ()
    {
        charactermover();
        angler();
        angledecider();
        Fire();
        Damage();
        HPregen();
	}

    void HPregen()
    {
        if (MusicSwitch.Song2Active && HP <= 20)
        {
            HP += 2 * Time.deltaTime;
        }
    }

    void charactermover()
    {
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * speed * Time.deltaTime);
            charAnim.SetBool("movingW", true);
            charAnim.SetBool("facingW", true);
            charAnim.SetBool("facingN", false);
            charAnim.SetBool("facingE", false);
            charAnim.SetBool("facingS", false);
            charAnim.SetBool("movingE", false);
            charAnim.SetBool("movingN", false);
            charAnim.SetBool("movingS", false);

            if (MusicSwitch.Song1Active)
            {
                charAnim.SetBool("Running", true);
                charAnim.SetBool("Walking", false);
                charAnim.SetBool("Healing", false);
            }
            else if (MusicSwitch.Song2Active)
            { 
                charAnim.SetBool("Healing", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Walking", false);
            }
            else if (MusicSwitch.Song0Active)
            {
                charAnim.SetBool("Walking", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Healing", false);
            }
                        
        }

        else if (Input.GetKeyUp(KeyCode.A))
        {
            charAnim.SetBool("movingW", false);
        }

        else if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
            charAnim.SetBool("movingE", true);
            charAnim.SetBool("facingE", true);
            charAnim.SetBool("facingN", false);
            charAnim.SetBool("facingW", false);
            charAnim.SetBool("facingS", false);
            charAnim.SetBool("movingW", false);
            charAnim.SetBool("movingN", false);
            charAnim.SetBool("movingS", false);

            if (MusicSwitch.Song1Active)
            {
                charAnim.SetBool("Running", true);
                charAnim.SetBool("Walking", false);
                charAnim.SetBool("Healing", false);
            }
            else if (MusicSwitch.Song2Active)
            {
                charAnim.SetBool("Healing", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Walking", false);
            }
            else if (MusicSwitch.Song0Active)
            {
                charAnim.SetBool("Walking", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Healing", false);
            }
        }

        else if (Input.GetKeyUp(KeyCode.D))
        {
            charAnim.SetBool("movingE", false);
        }

        else if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.up * speed * Time.deltaTime);
            charAnim.SetBool("movingN", true);
            charAnim.SetBool("facingN", true);
            charAnim.SetBool("facingW", false);
            charAnim.SetBool("facingE", false);
            charAnim.SetBool("facingS", false);
            charAnim.SetBool("movingE", false);
            charAnim.SetBool("movingW", false);
            charAnim.SetBool("movingS", false);

            if (MusicSwitch.Song1Active)
            {
                charAnim.SetBool("Running", true);
                charAnim.SetBool("Walking", false);
                charAnim.SetBool("Healing", false);
            }
            else if (MusicSwitch.Song2Active)
            {
                charAnim.SetBool("Healing", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Walking", false);
            }
            else if (MusicSwitch.Song0Active)
            {
                charAnim.SetBool("Walking", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Healing", false);
            }
        }

        else if (Input.GetKeyUp(KeyCode.W))
        {
            charAnim.SetBool("movingN", false);
        }

        else if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.down * speed * Time.deltaTime);
            charAnim.SetBool("movingS", true);
            charAnim.SetBool("facingS", true);
            charAnim.SetBool("facingN", false);
            charAnim.SetBool("facingE", false);
            charAnim.SetBool("facingW", false);
            charAnim.SetBool("movingE", false);
            charAnim.SetBool("movingN", false);
            charAnim.SetBool("movingW", false);

            if (MusicSwitch.Song1Active)
            {
                charAnim.SetBool("Running", true);
                charAnim.SetBool("Walking", false);
                charAnim.SetBool("Healing", false);
            }
            else if (MusicSwitch.Song2Active)
            {
                charAnim.SetBool("Healing", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Walking", false);
            }
            else if (MusicSwitch.Song0Active)
            {
                charAnim.SetBool("Walking", true);
                charAnim.SetBool("Running", false);
                charAnim.SetBool("Healing", false);
            }
        }

        else if (Input.GetKeyUp(KeyCode.S))
        {
            charAnim.SetBool("movingS", false);
        }

        else
        {
            charAnim.SetBool("Walking", false);
            charAnim.SetBool("Running", false);
            charAnim.SetBool("Healing", false);
        }

       

    }

    void angler()
    {
        //Obtains object position
        Vector2 positionOnScreen = Camera.main.WorldToViewportPoint(transform.position);

        //Obtains Mouse position
        Vector2 mouseOnScreen = (Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition);

        angle = AngleBetweenTwoPoints(positionOnScreen, mouseOnScreen);

        angle = Mathf.RoundToInt(angle);


    }

    //Uses a math function to get the angle in radials, then converts them to degrees
    float AngleBetweenTwoPoints(Vector3 a, Vector3 b)
    {
        return Mathf.Atan2(a.y - b.y, a.x - b.x) * Mathf.Rad2Deg;
    }

    void angledecider()
    {
        if (angle >= -30 && angle <= 30)
        {
            N = false;
            NE = false;
            E = false;
            SE = false;
            S = false;
            SW = false;
            W = true;
            NW = false;
            Shield.transform.localPosition = new Vector2(-0.332407f, 0f);
            Shield.transform.localRotation = Quaternion.Euler(0, 0, 180);
            Sword.transform.localPosition = new Vector2(-0.332407f, 0f);
            Sword.transform.localRotation = Quaternion.Euler(0, 0, 180);
            Gun.transform.localPosition = new Vector2(-0.332407f, 0f);
            Gun.transform.localRotation = Quaternion.Euler(0, 0, 180);
        }

        if (angle >= 30.01 && angle <= 60)
        {
            N = false;
            NE = false;
            E = false;
            SE = false;
            S = false;
            SW = true;
            W = false;
            NW = false;
            Shield.transform.localPosition = new Vector2(-0.332407f, -0.35f);
            Shield.transform.localRotation = Quaternion.Euler(0, 0, 225);
            Sword.transform.localPosition = new Vector2(-0.332407f, -0.35f);
            Sword.transform.localRotation = Quaternion.Euler(0, 0, 225);
            Gun.transform.localPosition = new Vector2(-0.332407f, -0.35f);
            Gun.transform.localRotation = Quaternion.Euler(0, 0, 225);
        }

        if (angle >= 60.01 && angle <= 120)
        {
            N = false;
            NE = false;
            E = false;
            SE = false;
            S = true;
            SW = false;
            W = false;
            NW = false;
            Shield.transform.localPosition = new Vector2(0f, -0.35f);
            Shield.transform.localRotation = Quaternion.Euler(0, 0, 270);
            Sword.transform.localPosition = new Vector2(0f, -0.35f);
            Sword.transform.localRotation = Quaternion.Euler(0, 0, 270);
            Gun.transform.localPosition = new Vector2(0f, -0.35f);
            Gun.transform.localRotation = Quaternion.Euler(0, 0, 270);
        }

        if (angle >= 120.01 && angle <= 150)
        {
            N = false;
            NE = false;
            E = false;
            SE = true;
            S = false;
            SW = false;
            W = false;
            NW = false;
            Shield.transform.localPosition = new Vector2(0.332407f, -0.35f);
            Shield.transform.localRotation = Quaternion.Euler(0, 0, 315);
            Sword.transform.localPosition = new Vector2(0.332407f, -0.35f);
            Sword.transform.localRotation = Quaternion.Euler(0, 0, 315);
            Gun.transform.localPosition = new Vector2(0.332407f, -0.35f);
            Gun.transform.localRotation = Quaternion.Euler(0, 0, 315);
        }

        if (angle >= 150.01 && angle <= 180 || angle >= -180 && angle <= -150.01)
        {
            N = false;
            NE = false;
            E = true;
            SE = false;
            S = false;
            SW = false;
            W = false;
            NW = false;
            Shield.transform.localPosition = new Vector2(0.332407f, 0f);
            Shield.transform.localRotation = Quaternion.Euler(0, 0, 0);
            Sword.transform.localPosition = new Vector2(0.332407f, 0f);
            Sword.transform.localRotation = Quaternion.Euler(0, 0, 0);
            Gun.transform.localPosition = new Vector2(0.332407f, 0f);
            Gun.transform.localRotation = Quaternion.Euler(0, 0, 0);
        }

        if (angle >= -150 && angle <= -120.01)
        {
            N = false;
            NE = true;
            E = false;
            SE = false;
            S = false;
            SW = false;
            W = false;
            NW = false;
            Shield.transform.localPosition = new Vector2(0.332407f, 0.35f);
            Shield.transform.localRotation = Quaternion.Euler(0, 0, 45);
            Sword.transform.localPosition = new Vector2(0.332407f, 0.35f);
            Sword.transform.localRotation = Quaternion.Euler(0, 0, 45);
            Gun.transform.localPosition = new Vector2(0.332407f, 0.35f);
            Gun.transform.localRotation = Quaternion.Euler(0, 0, 45);
        }

        if (angle >= -120 && angle <= -60.01)
        {
            N = true;
            NE = false;
            E = false;
            SE = false;
            S = false;
            SW = false;
            W = false;
            NW = false;
            Shield.transform.localPosition = new Vector2(0f, 0.35f);
            Shield.transform.localRotation = Quaternion.Euler(0, 0, 90);
            Sword.transform.localPosition = new Vector2(0f, 0.35f);
            Sword.transform.localRotation = Quaternion.Euler(0, 0, 90);
            Gun.transform.localPosition = new Vector2(0f, 0.35f);
            Gun.transform.localRotation = Quaternion.Euler(0, 0, 90);
        }

        if (angle >= -60 && angle <= -30.01)
        {
            N = false;
            NE = false;
            E = false;
            SE = false;
            S = false;
            SW = false;
            W = false;
            NW = true;
            Shield.transform.localPosition = new Vector2(-0.332407f, 0.35f);
            Shield.transform.localRotation = Quaternion.Euler(0, 0, 135);
            Sword.transform.localPosition = new Vector2(-0.332407f, 0.35f);
            Sword.transform.localRotation = Quaternion.Euler(0, 0, 135);
            Gun.transform.localPosition = new Vector2(-0.332407f, 0.35f);
            Gun.transform.localRotation = Quaternion.Euler(0, 0, 135);
        }
    }

    void Fire()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {            
            if (MusicSwitch.Song3Active)
            {
                if (!isAttacking)
                {
                    PlayFootstep();
                    isAttacking = true;
                    swordAnim.SetBool("isSwinging", true);
                    StartCoroutine("Swordattack");
                    
                }
            }
        }
    }
    public IEnumerator Swordattack()
    {
        SwordCol.enabled = true;
        yield return new WaitForSecondsRealtime(0.32f);
        swordAnim.SetBool("isSwinging", false);
        yield return new WaitForSecondsRealtime(0.3f);
        SwordCol.enabled = false;
        
        isAttacking = false;
    }

    void PlayFootstep()
    {
        randomInt = Random.Range(0, 4);

        if(randomInt == 0)
        {
            AudioS.pitch = Random.Range(0.9f, 1.2f);
            AudioS.PlayOneShot(footsteps[randomInt]);
        }
        else if (randomInt == 1)
        {
            AudioS.pitch = Random.Range(0.9f, 1.2f);
            AudioS.PlayOneShot(footsteps[randomInt]);
        }
        else if (randomInt == 2)
        {
            AudioS.pitch = Random.Range(0.9f, 1.2f);
            AudioS.PlayOneShot(footsteps[randomInt]);
        }
        else
        {
            AudioS.pitch = Random.Range(0.9f, 1.2f);
            AudioS.PlayOneShot(footsteps[randomInt]);
        }
    }

    void BlankStep()
    {
        //AudioS.Stop();
    }

    public void GunFire()
    {
        randomInt = Random.Range(0, 4);

        if (randomInt == 0)
        {
            gunFire.pitch = Random.Range(0.8f, 1.5f);
            gunFire.PlayOneShot(shots[randomInt]);
        }
        else if (randomInt == 1)
        {
            gunFire.pitch = Random.Range(0.8f, 1.5f);
            gunFire.PlayOneShot(shots[randomInt]);
        }
        else if (randomInt == 2)
        {
            gunFire.pitch = Random.Range(0.8f, 1.5f);
            gunFire.PlayOneShot(shots[randomInt]);
        }
        else
        {
            gunFire.pitch = Random.Range(0.8f, 1.5f);
            gunFire.PlayOneShot(shots[randomInt]);
        }
    }

    void Damage()
    {
        if (HP <= 0)
        {
            Destroy(gameObject);
        }

        if (isHurt)
        {
            isHurt = false;
            StartCoroutine(Invincibility());
        }
    }

    public IEnumerator Invincibility()
    {
        charactercol.enabled = false;
        charactersprite.enabled = false;
        yield return new WaitForSecondsRealtime(0.2f);
        charactersprite.enabled = true;
        yield return new WaitForSecondsRealtime(0.2f);
        charactersprite.enabled = false;
        yield return new WaitForSecondsRealtime(0.2f);
        charactersprite.enabled = true;
        yield return new WaitForSecondsRealtime(0.2f);
        charactersprite.enabled = false;
        yield return new WaitForSecondsRealtime(0.2f);
        charactersprite.enabled = true;
        yield return new WaitForSecondsRealtime(0.2f);
        charactersprite.enabled = false;
        yield return new WaitForSecondsRealtime(0.2f);
        charactersprite.enabled = true;
        charactercol.enabled = true;
    }
}
