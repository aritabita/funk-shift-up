﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Cooldown_script : MonoBehaviour
{
    public Image circularSilder;            
    public float time;
    public static bool run = false;                      
    void Start()
    {
        circularSilder.fillAmount = 0f;
    }
    void Update()
    {
        if (run)
        {
            circularSilder.fillAmount += Time.deltaTime / time;
            if (circularSilder.fillAmount == 1f)
            {
                run = false;
            }
        }

    }

}