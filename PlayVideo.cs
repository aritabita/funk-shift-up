﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

[RequireComponent (typeof(AudioSource))]

public class PlayVideo : MonoBehaviour
{

    public MovieTexture movie;

    private AudioSource sound;

    

    // Use this for initialization
    void Start()
    {
        GetComponent<RawImage>().texture = movie as MovieTexture;
        sound = GetComponent<AudioSource>();
        sound.clip = movie.audioClip;
        sound.Play();
        movie.Play();
        
        //StartCoroutine("Wait");
    }

    // Update is called once per frame
    void Update()
    {

    }

    //IEnumerator Wait()
   // {
        //yield return new WaitForSecondsRealtime(0.2f);
     //   sound.Play();
     //   yield return null;
   // }

   
}
