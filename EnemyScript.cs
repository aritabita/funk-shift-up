﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;

public class EnemyScript : MonoBehaviour {

    public GameObject Player;
    public int enemySpeed;
    private int enemyHP = 10;
    public static int enemyCount;
    public Animator enemyAnim;
    public AudioSource werewolf;
    public AudioClip wfAttack;
    public AudioClip wfAttack2;
    public AudioSource playerGetsHit;
    public AudioClip pGetsHit;
    public AudioSource wfIsHit;
    public AudioClip[] wfTakesDmg;
    public AudioSource wfIsDead;
    public AudioClip wfDeath;
    public bool enemyCountDown = true;
    public float dist;

    private int randomInt;

	// Use this for initialization
	void Start ()
    {
        enemyCount += 1;
        enemyAnim.SetBool("isWalking", true);

	}
	
	// Update is called once per frame
	void Update ()
    {
        EnemyMovement();
        HP();
       
	}

    void HP()
    {
        if (enemyHP <= 0)
        {
            if (enemyCountDown)
            {
                enemyCount -= 1;
                enemyCountDown = false;
            }
            gameObject.GetComponent<PolygonCollider2D>().enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            Destroy(gameObject, wfDeath.length);                      
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            
            enemyHP -= 1;

            EnemyIsHit();

            if(enemyHP == 0)
            {
                wfIsDead.pitch = Random.Range(0.7f, 1.4f);
                wfIsDead.PlayOneShot(wfDeath);
            }
        }

        if (other.gameObject.tag == "Sword")
        {
            enemyHP -= 2;

            EnemyIsHit();

            if (enemyHP == 0 || enemyHP == -1)
            {
                wfIsDead.pitch = Random.Range(0.7f, 1.4f);
                wfIsDead.PlayOneShot(wfDeath);
            }
        }

        else if (other.gameObject.tag == "Player")
        {
            Player = GameObject.Find("Mr.Boxyboxpantsman");

            if (transform.position.y > Player.transform.position.y)
            {
                CharacterScript.characterrigid.transform.Translate(Vector3.down * 10 * Time.deltaTime);
                CharacterScript.HP -= 0.5f;
                CharacterScript.isHurt = true;

                werewolf.pitch = Random.Range(0.9f, 1.3f);
                werewolf.PlayOneShot(wfAttack);
                playerGetsHit.pitch = Random.Range(0.9f, 1.1f);
                playerGetsHit.PlayOneShot(pGetsHit);
            }

            else
            {
                CharacterScript.characterrigid.transform.Translate(Vector3.up * 10 * Time.deltaTime);
                CharacterScript.HP -= 0.5f;
                CharacterScript.isHurt = true;

                werewolf.pitch = Random.Range(0.9f, 1.3f);
                werewolf.PlayOneShot(wfAttack2);
                playerGetsHit.pitch = Random.Range(0.9f, 1.1f);
                playerGetsHit.PlayOneShot(pGetsHit);
            }

            if (transform.position.x > Player.transform.position.x)
            {
                CharacterScript.characterrigid.transform.Translate(Vector3.left * 10 * Time.deltaTime);
                CharacterScript.HP -= 0.5f;
                CharacterScript.isHurt = true;

                werewolf.pitch = Random.Range(0.9f, 1.3f);
                werewolf.PlayOneShot(wfAttack2);
                playerGetsHit.pitch = Random.Range(0.9f, 1.1f);
                playerGetsHit.PlayOneShot(pGetsHit);
            }

            else
            {
                CharacterScript.characterrigid.transform.Translate(Vector3.right * 10 * Time.deltaTime);
                CharacterScript.HP -= 0.5f;
                CharacterScript.isHurt = true;

                werewolf.pitch = Random.Range(0.9f, 1.3f);
                werewolf.PlayOneShot(wfAttack);
                playerGetsHit.pitch = Random.Range(0.9f, 1.1f);
                playerGetsHit.PlayOneShot(pGetsHit);
            }

           
        }


    }

    void EnemyMovement()
    {
        Player = GameObject.Find("Mr.Boxyboxpantsman");

        if (Player != null)
        {
            dist = Vector3.Distance(Player.transform.position, transform.position);
            if (dist < 0.5)
            {
                enemyAnim.SetBool("isWalking", false);
                enemyAnim.SetBool("isAttacking", true);

            }

            else {
                if (transform.position.y > Player.transform.position.y)
                {
                    enemyAnim.SetBool("isAttacking", false);
                    enemyAnim.SetBool("isWalking", true);
                    transform.Translate(Vector3.down * enemySpeed * Time.deltaTime);
                }

                else
                {
                    enemyAnim.SetBool("isAttacking", false);
                    enemyAnim.SetBool("isWalking", true);
                    transform.Translate(Vector3.up * enemySpeed * Time.deltaTime);
                }

                if (transform.position.x > Player.transform.position.x)
                {
                    enemyAnim.SetBool("isAttacking", false);
                    enemyAnim.SetBool("isWalking", true);
                    transform.Translate(Vector3.left * enemySpeed * Time.deltaTime);
                    Vector3 difference = transform.position - Player.transform.position;
                    var distanceInX = Mathf.Abs(difference.x);
                    if (distanceInX > 0.5)
                    {
                        gameObject.transform.localScale = new Vector3(1, 1, 1);
                    }
                }

                else
                {
                    enemyAnim.SetBool("isAttacking", false);
                    enemyAnim.SetBool("isWalking", true);
                    transform.Translate(Vector3.right * enemySpeed * Time.deltaTime);
                    Vector3 difference = transform.position - Player.transform.position;
                    var distanceInX = Mathf.Abs(difference.x);
                    if (distanceInX > 0.5)
                    {
                        gameObject.transform.localScale = new Vector3(-1, 1, -1);
                    }
                }
            }
        }
        else
        {
            enemyAnim.SetBool("isAttacking", false);
            enemyAnim.SetBool("isWalking", false);
            enemyAnim.SetBool("isIdle", true);
        }
    }

    void EnemyIsHit()
    {
        randomInt = Random.Range(0, 3);

        if (randomInt == 0 && !wfIsHit.isPlaying)
        {
            wfIsHit.pitch = Random.Range(0.9f, 1.2f);
            wfIsHit.PlayOneShot(wfTakesDmg[randomInt]);
        }
        else if (randomInt == 1 && !wfIsHit.isPlaying)
        {
            wfIsHit.pitch = Random.Range(0.9f, 1.2f);
            wfIsHit.PlayOneShot(wfTakesDmg[randomInt]);
        }
        else if (randomInt == 2 && !wfIsHit.isPlaying)
        {
            wfIsHit.pitch = Random.Range(0.9f, 1.2f);
            wfIsHit.PlayOneShot(wfTakesDmg[randomInt]);
        }
        else if (randomInt == 3 && !wfIsHit.isPlaying)
        {
            wfIsHit.pitch = Random.Range(0.9f, 1.2f);
            wfIsHit.PlayOneShot(wfTakesDmg[randomInt]);
        }
    }

}
