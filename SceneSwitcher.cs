﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class SceneSwitcher : MonoBehaviour
{
    public string nextLevel;

    public AudioMixer mixer;

    float vol;

    //float fadeTime;

    void Start()
    {
        
        //StartCoroutine("MasterFadeIn");
       // StopCoroutine("ChangeLevel");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            //
            //StopCoroutine("ChangeLevel");
            StartCoroutine("ChangeLevel");
           // StopCoroutine("MasterFadeIn");
            
        }
       
    }



    public IEnumerator ChangeLevel()
    {
        print("TELEPORT");

        while (vol > -80f)
        {
            float fadeTime = GameObject.Find("Atmosphere").GetComponent<Fading>().BeginFade(1);
            //yield return new WaitForSeconds(fadeTime);
            vol -= Time.deltaTime * 20.8f;
            mixer.SetFloat("masterVol", vol);
            yield return null;
        }
        vol = -80f;

        mixer.SetFloat("masterVol", vol);

        Scene();
        
        
    }

    void Scene()
    {
        
        SceneManager.LoadScene(nextLevel, LoadSceneMode.Single);
    }

    
}



