﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MasterFadeIn : MonoBehaviour
{
    public AudioMixer mixer;

    float vol;

    // Use this for initialization
    void Start()
    {
        StartCoroutine("MasterIn");
    }

    public IEnumerator MasterIn()
    {
        while (vol < 5.0f)
        {
            vol += Time.deltaTime * 55.8f;
            mixer.SetFloat("masterVol", vol);
            yield return null;
        }

        vol = 5.0f;
        mixer.SetFloat("masterVol", vol);
    }
}
