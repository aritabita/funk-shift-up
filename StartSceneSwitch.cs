﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;

public class StartSceneSwitch : MonoBehaviour
{
    public string nextLevel;

    public AudioMixer mixer;

    float vol = -80.0f;

    void Start()
    {

        StartCoroutine("MasterFadeIn");
    }

    public void Clicked()
    {

       
            StartCoroutine("ChangeLevel");
        

    }

    public IEnumerator ChangeLevel()
    {
        print("TELEPORT");

        while (vol > -80f)
        {
            float fadeTime = GameObject.Find("GameObject").GetComponent<Fading>().BeginFade(1);
            //yield return new WaitForSeconds(fadeTime);
            vol -= Time.deltaTime * 20.8f;
            mixer.SetFloat("masterVol", vol);
            yield return null;
        }
        vol = -80f;

        mixer.SetFloat("masterVol", vol);

        Scene();


    }

    void Scene()
    {

        SceneManager.LoadScene(nextLevel, LoadSceneMode.Single);
    }

    public IEnumerator MasterFadeIn()
    {
        while (vol < 5.0f)
        {
            vol += Time.deltaTime * 205.8f;
            mixer.SetFloat("masterVol", vol);
            yield return null;
        }

        vol = 5.0f;
        mixer.SetFloat("masterVol", vol);
    }
}
