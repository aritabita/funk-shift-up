﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour {

    public Animator heart1Animator;
    public Animator heart2Animator;
    public Animator heart3Animator;
    public Animator heart4Animator;
    public Animator heart5Animator;
    public Image cool1;
    public Image cool2;
    public Image cool3;


    // Use this for initialization
    void Start()
    { 

        heart1Animator.SetBool("hurt1", false);
        heart2Animator.SetBool("hurt1", false);
        heart3Animator.SetBool("hurt1", false);
        heart4Animator.SetBool("hurt1", false);
        heart5Animator.SetBool("hurt1", false);
    }

    // Update is called once per frame
    void Update()
    {
        heartController();
        cooldissappear();
    }

    void heartController()
    {
        if (CharacterScript.HP > 19.5f)
        {
            heart5Animator.SetBool("hurt1", false);
        }

        else if (CharacterScript.HP > 18.5f)
        {
            heart5Animator.SetBool("hurt2", false);
            heart5Animator.SetBool("hurt1", true);
        }
        else if (CharacterScript.HP > 17.5)
        {
            heart5Animator.SetBool("hurt3", false);
            heart5Animator.SetBool("hurt2", true);

        }

        else if (CharacterScript.HP > 16.5)
        {
            heart5Animator.SetBool("hurt4", false);
            heart5Animator.SetBool("hurt3", true);
        }

        //heart 1 lost
        else if (CharacterScript.HP > 15.5)
        {
            heart4Animator.SetBool("hurt1", false);
            heart5Animator.SetBool("hurt4", true);
        }

        else if (CharacterScript.HP > 14.5)
        {
            heart4Animator.SetBool("hurt2", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt1", true);
        }
        else if (CharacterScript.HP > 13.5)
        {
            heart4Animator.SetBool("hurt3", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt2", true);
        }
        //heart 2 lost
        else if (CharacterScript.HP > 12.5)
        {
            heart4Animator.SetBool("hurt4", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt3", true);
        }
        //heart 2 lost
        else if (CharacterScript.HP > 11.5)
        {
            heart3Animator.SetBool("hurt1", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
        }
        else if (CharacterScript.HP > 10.5)
        {
            heart3Animator.SetBool("hurt2", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt1", true);
        }
        else if (CharacterScript.HP > 9.5)
        {
            heart3Animator.SetBool("hurt3", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt2", true);
        }
        
        else if (CharacterScript.HP > 8.5)
        {
            heart3Animator.SetBool("hurt4", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt3", true);
        }
        //heart 3 lost
        else if (CharacterScript.HP > 7.5)
        {
            heart2Animator.SetBool("hurt1", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt4", true);
        }
        else if (CharacterScript.HP > 6.5)
        {
            heart2Animator.SetBool("hurt2", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt4", true);
            heart2Animator.SetBool("hurt1", true);
        }
        else if (CharacterScript.HP > 5.5)
        {
            heart2Animator.SetBool("hurt3", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt4", true);
            heart2Animator.SetBool("hurt2", true);
        }
     
        else if (CharacterScript.HP > 4.5)
        {
            heart2Animator.SetBool("hurt4", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt4", true);
            heart2Animator.SetBool("hurt3", true);
        }
        //heart 4 lost
        else if (CharacterScript.HP > 3.5)
        {
            heart1Animator.SetBool("hurt1", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt4", true);
            heart2Animator.SetBool("hurt4", true);
        }
        else if (CharacterScript.HP > 2.5)
        {
            heart1Animator.SetBool("hurt2", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt4", true);
            heart2Animator.SetBool("hurt4", true);
            heart1Animator.SetBool("hurt1", true);
        }
        else if (CharacterScript.HP > 1.5)
        {
            heart1Animator.SetBool("hurt3", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt4", true);
            heart2Animator.SetBool("hurt4", true);
            heart1Animator.SetBool("hurt2", true);
        }
     
        else if (CharacterScript.HP > 0.5)
        {
            heart1Animator.SetBool("hurt4", false);
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt4", true);
            heart2Animator.SetBool("hurt4", true);
            heart1Animator.SetBool("hurt3", true);
        }
        //dead
        else
        {
            heart5Animator.SetBool("hurt4", true);
            heart4Animator.SetBool("hurt4", true);
            heart3Animator.SetBool("hurt4", true);
            heart2Animator.SetBool("hurt4", true);
            heart1Animator.SetBool("hurt4", true);
        }

    }

    void cooldissappear()
    {
        if (MusicSwitch.timer == 0)
        {
            cool1.fillAmount = 0f;
            cool2.fillAmount = 0f;
            cool3.fillAmount = 0f;
        }
    }
}
