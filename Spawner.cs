﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public Rigidbody2D Enemy;
    public GameObject Activator;
    public int SpawnerHp = 20;

    public GameObject audioObject;
    public AudioSource shutDown;
    public AudioClip shutD;
    public GameObject teleporter;
    

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("EnemySpawn", 1, 13);

        shutDown = audioObject.gameObject.GetComponent<AudioSource>();

        
        
        	
	}
	
	// Update is called once per frame
	void Update () 
    {
        SpawnerHealth();
	}

    void EnemySpawn()
    {
        Debug.Log(EnemyScript.enemyCount);
        if (EnemyScript.enemyCount <= 8)
        {
            Rigidbody2D enemyInstance = new Rigidbody2D();
            //enemy 1
            enemyInstance = Instantiate(Enemy);
            Physics2D.IgnoreCollision(enemyInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            enemyInstance.transform.parent = transform;
            enemyInstance.transform.localPosition = new Vector3(-0.405f, 0.176f, 0);
            enemyInstance.transform.rotation = Quaternion.Euler(0, 0, 0);
            //enemyInstance.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            //enemy 2
            enemyInstance = Instantiate(Enemy);
            Physics2D.IgnoreCollision(enemyInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            enemyInstance.transform.parent = transform;
            enemyInstance.transform.localPosition = new Vector3(0.382f, 0.176f, 0);
            enemyInstance.transform.rotation = Quaternion.Euler(0, 0, 0);
            //enemyInstance.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            //enemy 3
            enemyInstance = Instantiate(Enemy);
            Physics2D.IgnoreCollision(enemyInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            enemyInstance.transform.parent = transform;
            enemyInstance.transform.localPosition = new Vector3(0.382f, -0.547f, 0);
            enemyInstance.transform.rotation = Quaternion.Euler(0, 0, 0);
            //enemyInstance.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
            //enemy 4
            enemyInstance = Instantiate(Enemy);
            Physics2D.IgnoreCollision(enemyInstance.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            enemyInstance.transform.parent = transform;
            enemyInstance.transform.localPosition = new Vector3(-0.405f, -0.547f, 0);
            enemyInstance.transform.rotation = Quaternion.Euler(0, 0, 0);
            //enemyInstance.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
        }
    }

    void SpawnerHealth()
    {
        
        if (SpawnerHp <= 0)
        {
            shutDown.PlayOneShot(shutD);

            teleporter.SetActive(true);

            this.gameObject.SetActive(false);
            Destroy(gameObject);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            print(SpawnerHp);
            SpawnerHp -= 1;
            
        }

        if (other.gameObject.tag == "Sword")
        {
            print(SpawnerHp);
            SpawnerHp -= 2;
        }
    }

    //void OnCollisionStay2D(Collision2D other)
    //{
    //    if (other.gameObject.tag == "Sword")
    //    {
    //        print(SpawnerHp);
    //        SpawnerHp -= 2;
    //    }
    //}


}
