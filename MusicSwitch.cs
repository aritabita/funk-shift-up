﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicSwitch : MonoBehaviour {

    public static int timer = 0;
    public AudioSource Song1;
    public AudioSource Song2;
    public AudioSource Song3;
    public CharacterScript charin;
    public GameObject player;
    public static bool Song1Active;
    public static bool Song2Active;
    public static bool Song3Active;
    public static bool Song0Active;

    // Use this for initialization
    void Start ()
    {
        Song1Active = false;
        Song2Active = false;
        Song0Active = true;
        charin = player.GetComponent<CharacterScript>();
        charin.Shield.SetActive(false); 
        Song1.volume = 0;
        Song2.volume = 0;
        Song3.volume = 0;
	}
	
	// Update is called once per frame
	void Update ()
    {
        SongSwitcher();
	}

    void SongSwitcher()
    {
        if (timer == 0)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                StopCoroutine("outFade2");
                StopCoroutine("outFade1");
                StopCoroutine("outFade3");
                StopCoroutine("Song1Volume");
                Cooldown_script.run = true;
                Song1Active = true;
                Song2Active = false;
                Song3Active = false;
                charin.Gun.SetActive(true);
                charin.Shield.SetActive(false);
                charin.Sword.SetActive(false);
                timer = 16;
                StartCoroutine("Song1Volume");
                StartCoroutine("inFade1");
                StartCoroutine("smallFade2");
                StartCoroutine("smallFade3");

            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                StopCoroutine("outFade1");
                StopCoroutine("outFade2");
                StopCoroutine("outFade3");
                StopCoroutine("Song2Volume");
                Cooldown_script.run = true;
                Song1Active = false;
                Song2Active = true;
                Song3Active = false;
                charin.Shield.SetActive(true);
                charin.Sword.SetActive(false);
                charin.Gun.SetActive(false);
                timer = 16;
                StartCoroutine("Song2Volume");
                StartCoroutine("inFade2");
                StartCoroutine("smallFade1");
                StartCoroutine("smallFade3");


            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                StopCoroutine("outFade1");
                StopCoroutine("outFade2");
                StopCoroutine("outFade3");
                StopCoroutine("Song3Volume");
                Cooldown_script.run = true;
                Song1Active = false;
                Song2Active = false;
                Song3Active = true;
                charin.Shield.SetActive(false);
                charin.Gun.SetActive(false);
                charin.Sword.SetActive(true);
                timer = 16;
                StartCoroutine("Song3Volume");
                StartCoroutine("inFade3");
                StartCoroutine("smallFade1");
                StartCoroutine("smallFade2");
            }
                       
        }      
    }

    public IEnumerator Song1Volume()
    {      
        StartCoroutine(timercontrol());
        
        CharacterScript.speed = 4.0f;
        //StartCoroutine("outFade2");
        //Song2.volume = 0;
        //StopCoroutine("outFade2");
        // Song1.volume = 1;
        //yield return new WaitForSecondsRealtime(5.0f);
        //Song1.volume = 0.75f;
        yield return new WaitForSecondsRealtime(30.0f);       
        StartCoroutine("outFade1");
            
        
        
        



    }

   

    

    public IEnumerator Song2Volume()
    {        
        StartCoroutine(timercontrol());
        
        CharacterScript.speed = 2.0f;
        //StartCoroutine("outFade1");
        //Song1.volume = 0;
        //StopCoroutine("outFade1");
        //Song2.volume = 1;
        //yield return new WaitForSecondsRealtime(5.0f);
        //Song2.volume = 0.75f;
        yield return new WaitForSecondsRealtime(30.0f);
        StartCoroutine("outFade2");
        


    }

    public IEnumerator Song3Volume()
    {
        StartCoroutine(timercontrol());

        CharacterScript.speed = 3.5f;
        //StartCoroutine("outFade2");
        //Song2.volume = 0;
        //StopCoroutine("outFade2");
        // Song1.volume = 1;
        //yield return new WaitForSecondsRealtime(5.0f);
        //Song1.volume = 0.75f;
        yield return new WaitForSecondsRealtime(30.0f);
        StartCoroutine("outFade3");







    }




    public IEnumerator timercontrol()
    {
        while (timer > 1)
        {
            timer -= 1;
            yield return new WaitForSecondsRealtime(1);
        }
        
        timer = 0;
        StopCoroutine(timercontrol());
    }

  
    IEnumerator outFade1()
    {
        
        while (Song1.volume > 0.01f)
        {
            Song1.volume -= Time.deltaTime / 3.0f;
            

            Song1Active = false;
            Song2Active = false;
            Song3Active = false;
            Song0Active = true;
            CharacterScript.speed = 3.0f;
            charin.Shield.SetActive(false);
            charin.Sword.SetActive(false);
            charin.Gun.SetActive(false);
            yield return null;
            





        }
        Song1.volume = 0;



    }

    IEnumerator outFade2()
    {
        
        while (Song2.volume > 0.01f)
        {
            Song2.volume -= Time.deltaTime / 3.0f;
            
            
            Song1Active = false;
            Song2Active = false;
            Song3Active = false;
            Song0Active = true;
            CharacterScript.speed = 3.0f;
            charin.Shield.SetActive(false);
            charin.Sword.SetActive(false);
            charin.Gun.SetActive(false);
            yield return null;
            




        }
        Song2.volume = 0;
    }

    IEnumerator outFade3()
    {

        while (Song3.volume > 0.01f)
        {
            Song3.volume -= Time.deltaTime / 3.0f;


            Song1Active = false;
            Song2Active = false;
            Song3Active = false;
            Song0Active = true;
            CharacterScript.speed = 3.0f;
            charin.Shield.SetActive(false);
            charin.Sword.SetActive(false);
            charin.Gun.SetActive(false);
            yield return null;






        }
        Song3.volume = 0;



    }

    IEnumerator smallFade1()
    {
        while (Song1.volume > 0.01f)
        {
            Song1.volume -= Time.deltaTime / 3.0f;
            
            yield return null;
        }
        Song1.volume = 0;
    }

    IEnumerator smallFade2()
    {
        while (Song2.volume > 0.01f)
        {
            Song2.volume -= Time.deltaTime / 3.0f;
            
            yield return null;
        }
        Song2.volume = 0;
    }

    IEnumerator smallFade3()
    {
        while (Song3.volume > 0.01f)
        {
            Song3.volume -= Time.deltaTime / 3.0f;

            yield return null;
        }
        Song3.volume = 0;
    }

    IEnumerator inFade2()
    {
        while (Song2.volume < 1.0f)
        {
            Song2.volume += Time.deltaTime / 1.75f;
            yield return null;
        }
        Song2.volume = 1;
    }

    IEnumerator inFade1()
    {
        while (Song1.volume < 1.0f)
        {
            Song1.volume += Time.deltaTime / 1.75f;
            yield return null;
        }
        Song1.volume = 1;
    }

    IEnumerator inFade3()
    {
        while (Song3.volume < 1.0f)
        {
            Song3.volume += Time.deltaTime / 1.75f;
            yield return null;
        }
        Song3.volume = 1;
    }
}
