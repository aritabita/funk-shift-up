## Introducing Funk Shift Up, a Unity game that mixes music with action!

**Note: This project was finished on May 2017. The following repository is simply for portfolio reasons. As such, all assets except for the scripts are missing.**

In Funk Shift Up, you take control of a young woman in a desolate world. Armed with nothing but her iPod and her bravery, she ventures to the unknown, hoping to gain some insight on what has happened to this world.

Features:

- Three different beautiful levels.
- Amazing music that powers the character up!
- Three different modes depending on what song is playing:
	- Move at the speed of sound and shoot bullets!
	- Carefully predict your enemies' actions and slice them in two with your trusty sword!
	- Slowly withdraw from the fight with your impervious shield, which also regenerates your health!


![Game Screenshot](https://cdn.discordapp.com/attachments/291644354173599745/452258470885064705/unknown.png)