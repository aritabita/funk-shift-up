﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerActivator : MonoBehaviour
{

    public static bool Activated = false;

    public GameObject Spawner;

    public AudioSource spawnerSound;
    public AudioSource wfSource;
    public AudioClip wfStart;

    bool played = false;

  

    void Start()
    {
        //spawnerSound = Spawner.gameObject.GetComponent<AudioSource>();
        Activated = false;
              
    }

    void Update()
    {
        if (Activated == true && Spawner != null)
        {
            Spawner.SetActive(true);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Entered");           

            Activated = true;

            
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            spawnerSound.Play();

            if(!played)
            {
                wfSource.PlayOneShot(wfStart);
                played = true;
            }
            
        }
    }
}
