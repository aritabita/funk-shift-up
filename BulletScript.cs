﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour {


    void Start()
    {
        transform.parent = null;
        StartCoroutine(Selfdestruct());
    }


    void OnCollisionEnter2D(Collision2D other)
    {
        Destroy(gameObject);

    }

    public IEnumerator Selfdestruct()
    {
        yield return new WaitForSecondsRealtime(5);
        Destroy(gameObject);
    }
}
